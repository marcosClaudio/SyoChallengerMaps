var myApp = angular.module("myApp", []);
    myApp.directive("syoDirective", function() {
      return {
        restrict:'E',
        template:'<div></div>',
        replace:true,
        link:function($scope, $element, $attrs) {
          var myLatLng = new google.maps.LatLng(-29.6836549,-51.4572224);
          var mapOptions = {
            center: myLatLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.SATELLITE
          };
        var map = new google.maps.Map(document.getElementById($attrs.id),
            mapOptions);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: "Localização da Syonet."
        });
        marker.setMap(map);
        }
      };
    });